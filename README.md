# Hardening SSH

All edits below should be made to /etc/ssh/sshd_config
----
### Check prtocol
First, make sure that Protocol is set to 2. It can be either 1 or 2. The current default is 2.

### Disable root login
Change ```PermitRootLogin yes``` to ```PermitRootLogin no```

But please make sure you have another user which you can use to ssh into the server. It is also good idea that this user has sudo permissions.

### Disconnect Idle Sessions
Set:
```
ClientAliveInterval 300
ClientAliveCountMax 2
```

### Whitelist users
This is optional and I don't always do it. But still, you can specifically set which users can log in via SSH by adding the following:
```
AllowUsers [username]
```

### Change Ports

Honestly. I was surprised when I first saw what an efect this has. Usually, when port is set to 22, you'll get from couple 100's to couple 1000's failed logins per day. By changing the port you get 0 to maybe 10 per day.

So, change this:
```
Port 22
```
to something random like:
```
Port  8654
```
For the ease of management, I still like to keep the same port setting across my servers.

### SSH keys

Please, youse ssh-keys to access your servers whenever possible. Password login is a bad idea. If you don't know what an SSH key is or how to generate them, look [here](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-2).


